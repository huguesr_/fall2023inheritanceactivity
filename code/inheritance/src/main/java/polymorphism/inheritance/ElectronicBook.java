package polymorphism.inheritance;

public class ElectronicBook extends Book{
    private int numberBytes;

    public ElectronicBook(String title, String author, int bytes){
        super(title, author);
        this.numberBytes = bytes;
    }

    public int getNumberBytes(){
        return this.numberBytes;
    }

    public String toString(){
        String toReturn = "Title: "+this.title+", Author: "+getAuthor()+", NumBytes: "+this.numberBytes;
        return toReturn;
    }
}
