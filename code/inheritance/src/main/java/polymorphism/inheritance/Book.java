package polymorphism.inheritance;

public class Book {
    protected String title;
    private String author;

    public Book(String title, String author){
        this.title = title;
        this.author = author;
    }

    public String getAuthor(){
        return this.author;
    }
    public String getTitle(){
        return this.title;
    }

    public String toString(){
        String toReturn = "Title: "+this.title+", Author: "+this.author;
        return toReturn;
    }
}
