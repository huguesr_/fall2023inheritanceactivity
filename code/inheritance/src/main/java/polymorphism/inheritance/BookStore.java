package polymorphism.inheritance;

public class BookStore {
    public static void main(String[] args){
        Book[] bookArray = new Book[5];
        bookArray[0] = new Book("Life2", "Mahz");
        bookArray[2] = new Book("Vie4", "Bob");
        bookArray[1] = new ElectronicBook("Water", "Arthur", 45);
        bookArray[3] = new ElectronicBook("Fire", "Nicolas", 32);
        bookArray[4] = new ElectronicBook("Air", "Daniel", 88);

        for (int i =0; i<bookArray.length; i++){
            System.out.println(bookArray[i]);
        }
        //System.out.println(bookArray[1].getNumberBytes());
        //ElectronicBook testEB = (ElectronicBook)bookArray[1];
        //System.out.println(testEB.getNumberBytes());
        //ElectronicBook testB = (ElectronicBook)bookArray[0];
        //System.out.println(testB.getNumberBytes());
    }
}
